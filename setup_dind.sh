#!/usr/bin/env bash
declare -A ARCHS=( ["x86_64"]="amd64" ["aarch64"]="arm64" )
mkdir -vp ~/.docker/cli-plugins/
curl --silent -L "https://github.com/docker/buildx/releases/download/v0.5.1/buildx-v0.5.1.linux-${ARCHS[$(uname -m)]}" \
  > ~/.docker/cli-plugins/docker-buildx
curl --silent -L "https://github.com/regclient/regclient/releases/download/v0.4.1/regctl-linux-${ARCHS[$(uname -m)]}" \
  > /usr/bin/regctl
curl --silent -L "https://github.com/estesp/manifest-tool/releases/download/v2.0.3/binaries-manifest-tool-2.0.3.tar.gz" \
  | tar -xz manifest-tool-linux-${ARCHS[$(uname -m)]}
mv manifest-tool-linux-${ARCHS[$(uname -m)]} /usr/bin/manifest-tool
chmod a+x ~/.docker/cli-plugins/docker-buildx
chmod a+x /usr/bin/regctl
chmod a+x /usr/bin/manifest-tool
docker run --privileged --rm tonistiigi/binfmt -install arm64,amd64
